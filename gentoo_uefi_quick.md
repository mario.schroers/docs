# Gentoo Linux AMD Ryzen 5950X
# Install Script

## 1. Disk Partitioning
`sudo su`

`gdisk /dev/sda`
```
2
n
1
<ENTER>
+203M
ef00
n
2
<ENTER>
+4G
8200
n
3
<ENTER>
+50G
8300
n
4
<ENTER>
<ENTER>
8300
w
y
```

```sh
# Alternatively – Paste this block into Konsole:

(
    echo 2;
    echo n;
    echo 1;
    echo ;
    echo +203M;
    echo ef00;
    echo n;
    echo 2;
    echo ;
    echo +4G;
    echo 8200;
    echo n;
    echo 3;
    echo ;
    echo +50G
    echo 8300
    echo n;
    echo 4;
    echo ;
    echo ;
    echo 8300;
    echo w;
    echo y
) | gdisk /dev/sda

```


## 2. File Systems
```sh
mkfs.fat -F32 -n EFI /dev/sda1
mkswap -L SWAP /dev/sda2
mkfs.xfs -f -m bigtime=1 -L ROOT /dev/sda3
mkfs.xfs -f -m bigtime=1 -L HOME /dev/sda4
swapon /dev/sda2
```

## 3. Creating and Mounting Partitions
```sh
mkdir /mnt/gentoo
mount /dev/sda3 /mnt/gentoo
mkdir /mnt/gentoo/boot
mount /dev/sda1 /mnt/gentoo/boot
mkdir /mnt/gentoo/home
mount /dev/sda4 /mnt/gentoo/home
cd /mnt/gentoo
```

## 4. Download, Extract And Purge Tarball
```sh
LATEST=$(wget --quiet https://gentoo.osuosl.org/releases/amd64/autobuilds/latest-stage3-amd64-openrc.txt && grep "stage3-amd64-openrc" latest-stage3-amd64-openrc.txt | cut -d' ' -f1)
wget "http://distfiles.gentoo.org/releases/amd64/autobuilds/$LATEST"
tar xpvf stage3-*.tar.xz --xattrs-include='*.*' --numeric-owner
rm stage3*
```

## 5. Makeopts & USE Flags
```sh
echo 'MAKEOPTS="-j32"' >> /mnt/gentoo/etc/portage/make.conf
echo 'USE="-bluetooth -gnome -kde -libarchive -qtwebwngine -systemd alsa cups dbus elogind icu jack libinput libglvnd minizip nvidia opengl pdf pulseaudio synaptics text udisks X"' >> /mnt/gentoo/etc/portage/make.conf
echo 'INPUT_DEVICES="libinput synaptics"' >> /mnt/gentoo/etc/portage/make.conf
echo 'VIDEO_CARDS="nvidia"' >> /mnt/gentoo/etc/portage/make.conf
```

## For Binary Packages (2024)
`/etc/portage/make.conf`

```sh
EMERGE_DEFAULT_OPTS="${EMERGE_DEFAULT_OPTS} --getbinpkg --jobs=4 --load-average=4"
FEATURES="${FEATURES} binpkg-request-signature"
BINPKG_FORMAT="gpkg"
```

`/etc/portage/binrepos.conf/gentoobinhost.conf`

```sh
[gentoobinhost]
priority = 9999
sync-uri = http://ftp.agdsn.de/gentoo/releases/amd64/binpackages/17.1/x86-64/
```

`/etc/portage/make.conf`

```sh
COMMON_FLAGS="-O2 -pipe"
CFLAGS="${COMMON_FLAGS}"
CXXFLAGS="${COMMON_FLAGS}"
FCFLAGS="${COMMON_FLAGS}"
FFLAGS="${COMMON_FLAGS}"

LC_MESSAGES=C.utf8

EMERGE_DEFAULT_OPTS="${EMERGE_DEFAULT_OPTS} --jobs=4 --load-average=4 --getbinpkg"
FEATURES="${FEATURES} binpkg-request-signature"
BINPKG_FORMAT="gpkg"
```

## 6. Rank And Select Portage Mirrors
```sh
mirrorselect -c 'Germany' -s 5 >> /mnt/gentoo/etc/portage/make.conf
```

## 7. Prepare Repo Folders
```sh
mkdir -p /mnt/gentoo/etc/portage/repos.conf
cp /mnt/gentoo/usr/share/portage/config/repos.conf /mnt/gentoo/etc/portage/repos.conf/gentoo.conf
```

## 8. Network Access
```sh
cp --dereference /etc/resolv.conf /mnt/gentoo/etc/
```

## 9. Copy Kernel And Initramfs From Live ISO
```sh
cp /boot/System* /mnt/gentoo/boot
cp /boot/initramfs* /mnt/gentoo/boot
cp /boot/vmlinuz* /mnt/gentoo/boot
chmod +x /mnt/gentoo/boot/*
mkdir -p /mnt/gentoo/boot/efi/boot/
cp /mnt/gentoo/boot/vmlinuz-* /mnt/gentoo/boot/efi/boot/
```

## 10. Mount `proc`, `sys`, `dev`, `run`
```sh
mount --types proc /proc /mnt/gentoo/proc
mount --rbind /sys /mnt/gentoo/sys
mount --make-rslave /mnt/gentoo/sys
mount --rbind /dev /mnt/gentoo/dev
mount --make-rslave /mnt/gentoo/dev
mount --bind /run /mnt/gentoo/run
mount --make-slave /mnt/gentoo/run
```

## 11. Change Root
```sh
chroot /mnt/gentoo /bin/bash
source /etc/profile
RED='\033[1;31m'
NC='\033[0m'
export PS1="(${RED}chroot${NC}) ${PS1}"
```

## 12. Emerge Webrsync 
```sh
emerge-webrsync
eselect news list
yes | emerge --verbose --update --deep --newuse @world
```

## 13. Set Timezone
```sh
ls /usr/share/zoneinfo
echo "Europe/Berlin" > /etc/timezone
emerge --config sys-libs/timezone-data
```

## 14. Generate Locales
```sh
printf "en_US.UTF-8 UTF-8\nde_DE.UTF-8 UTF-8\n" > /etc/locale.gen
locale-gen
eselect locale list
eselect locale set <N>
```

## 15. Env-Update
```sh
env-update && source /etc/profile && export PS1="(${RED}chroot${NC}) ${PS1}"
```

## 16. Install Network Time Protocol
```sh
emerge net-misc/ntp
```

- Settings for file `/etc/conf.d/ntp-client`:
```sh
NTPCLIENT_CMD="ntpdate"
NTPCLIENT_OPTS="-s -b -u \
	0.gentoo.pool.ntp.org 1.gentoo.pool.ntp.org \
	2.gentoo.pool.ntp.org 3.gentoo.pool.ntp.org"
```

- Run ntp-update manually:
```sh
ntpdate -b -u 0.gentoo.pool.ntp.org
```

- Enable OpenRC NTP Service:
```sh
rc-service ntp-client start
rc-update add ntp-client default
```

## 17. Configure Keywords & Licenses
```sh
echo 'ACCEPT_KEYWORDS="amd64"' >> /etc/portage/make.conf
echo 'ACCEPT_LICENSE="*"' >> /etc/portage/make.conf
```

## 18. Install Kernel Sources & Genkernel
```sh
emerge sys-kernel/linux-firmware
emerge sys-kernel/gentoo-sources
emerge sys-apps/pciutils
emerge sys-kernel/genkernel
eselect kernel list
eselect kernel set 1
```

## 19. Genfstab
```sh
wget 'https://raw.githubusercontent.com/glacion/genfstab/master/genfstab'
chmod +x genfstab
./genfstab -Up ./ > /etc/fstab
cat /etc/fstab
rm genfstab
```

## 20. Genkernel
```sh
ls -l /usr/src/linux
cd /usr/src/linux
genkernel all
```

## 21. Prepare Boot Images
```sh
ls /boot/vmlinu* /boot/initramfs*
mkdir -p /boot/efi/boot
cp /boot/vmlinuz-* /boot/efi/boot/bootx64.efi
mount -o remount,rw /sys/firmware/efi/efivars/
```

## 22. Install GRUB
```sh
echo 'GRUB_PLATFORMS="efi-64"' >> /etc/portage/make.conf
emerge sys-boot/grub
grub-install --target=x86_64-efi --efi-directory=/boot --removable
grub-mkconfig -o /boot/grub/grub.cfg
nvim /etc/default/grub
GRUB_TERMINAL=gfxterm
GRUB_GFXMODE=1920x1200
grub-mkconfig -o /boot/grub/grub.cfg "$@"
```

## 23. Verify GRUB UEFI Boot Entry
```sh
awk -F\' '/menuentry / {print $2}' /boot/grub/grub.cfg
```

## 24. Set Hostname
```sh
echo "hostname" > /etc/hostname
```

## 25. Revert Default Password Complexity Requirements
```sh
sed -i "s/24,11,8,7/1,1,1,1/g" /etc/security/passwdqc.conf
sed -i "s/enforce=everyone/enforce=none/g" /etc/security/passwdqc.conf
```

## 26. Set root Password
```sh
passwd
```

## 27. Create Local User
```sh
useradd -m -G users,wheel,audio,video -s /bin/bash username
passwd username
```

## 28. `doas` Privilege Escalation (sudo alternative)
```sh
printf "# enable persist flag for doas\napp-admin/doas		persist\n" > /etc/portage/package.use/doas
echo 'app-admin/doas	-persist' >> /etc/portage/profile/package.use.mask
emerge app-admin/doas
printf "permit :wheel\npermit persist :wheel\n" > /etc/doas.conf
```

## 29. `dhcpcd` Automatic Network Connection (`eth`)
```sh
emerge net-misc/dhcpcd
rc-update add dhcpcd default
rc-service dhcpcd start
```

## 30. Text Editor: Neovim
```sh
emerge app-editors/neovim
emerge dev-python/pynvim
emerge x11-misc/xsel
```

## 31. Display Server: X11
```sh
emerge x11-base/xorg-server
emerge x11-drivers/nvidia-drivers
emerge x11-apps/xinit x11-apps/setxkbmap x11-apps/xmodmap x11-misc/xdg-user-dirs x11-misc/xdg-user-dirs-gtk
libtool --finish /usr/lib64/xorg/modules/drivers
```

## 32. Window Manager, Terminal, Launcher
```sh
emerge x11-wm/dwm x11-terms/st x11-misc/dmenu
```

## 33. Pcocess Management: D-Bus
```sh
emerge sys-apps/dbus
/etc/init.d/dbus start
rc-update add dbus default
```

## 34. Changed-Use Update
```sh
emerge --changed-use --deep @world
emerge --depclean
```

## 35. Update `hosts` File
- Add to file `/etc/hosts`:

```sh
127.0.0.1   <hostname>
```

## 36. Enable `elogind` Service
```sh
emerge sys-auth/elogind
rc-update add elogind boot
```
## 37. Allow poweroff, reboot, shutdown For Regular Users
```sh
chmod +s /sbin/poweroff
chmod +s /sbin/reboot
chmod +s /sbin/shutdown
```

## 38. Hardware & Process Monitoring, System Info
```sh
emerge sys-apps/lshw
emerge sys-process/htop
emerge app-misc/neofetch
```

## 39. Audio Support
```sh
emerge media-libs/alsa-lib media-sound/alsa-utils
rc-service alsasound start
rc-update add alsasound boot
emerge media-sound/pulseaudio media-sound/pavucontrol
```

## 40. System Fonts
```sh
emerge media-fonts/source-code-pro media-fonts/source-sans media-fonts/source-serif media-fonts/jetbrains-mono media-fonts/ibm-plex media-fonts/roboto media-fonts/ubuntu-font-family
```

## 41. Exit `chroot` & Reboot
```sh
exit
reboot
```
